(function() {
  'use strict';

  const https = require('https');
  const Promise = require('promise');

  function submitGetRequest(url) {
    return new Promise((resolve, reject) => {
      https.get(url, res => {
        
        let responseData = '';
        
        res.setEncoding('utf8');
        res.on('data', data => {
          responseData += data;
        })
        .on('end', () => {
          resolve(JSON.parse(responseData));
        })
        .on('error', err => {
          reject(err);
        });
      });
    });
  }

  module.exports = {

    getSlackUserName: function getSlackUserName (accessToken, slackUserId) {
      const me = this;
      return new Promise((resolve, reject) => {
        me.getUsers(accessToken)
            .then(users => {
              const slackUser = users.find(user => user.id === slackUserId);
              resolve(slackUser.real_name);
            })
            .catch(err => { reject(err); });
      });
    },

    getChannels: function getChannels (accessToken) {
      return new Promise((resolve, reject) => {
        const queryParams = [
          'token=' + accessToken
        ];

        const url = 'https://slack.com/api/channels.list?' + queryParams.join('&');

        submitGetRequest(url)
            .then(response => {
              resolve(response.channels)
            })
            .catch(err => {
              console.error(err);
              reject(err);
            });
      });
    },

    getChannelById: function getChannelById (accessToken, channelId) {
      return new Promise((resolve, reject) => {
        this.getChannels(accessToken)
            .then(channels => {
              resolve(channels.filter(channel => channel.id === channelId)[0]);
            })
            .catch(err => {
              reject(err);
            });
      });
    },

    inviteUserToChannel: function inviteUserToChannel (accessToken, channelId, userId) {
      return new Promise((resolve, reject) => {
        const queryParams = [
          'token=' + accessToken,
          'channel=' + channelId,
          'user=' + userId
        ];

        const url = 'https://slack.com/api/channels.invite?' + queryParams.join('&');

        submitGetRequest(url)
            .then(response => {
              resolve(response);
            })
            .catch(err => {
              console.error(err);
              reject(err);
            });
      });
    },

    getUsers: function getUsers (accessToken) {
      return new Promise((resolve, reject) => {
        const queryParams = [
          'token=' + accessToken
        ];

        const url = 'https://slack.com/api/users.list?' + queryParams.join('&');

        submitGetRequest(url)
            .then(response => {
              resolve(response.members)
            })
            .catch(err => {
              console.error(err);
              reject(err);
            });
      });
    }
  };
})();