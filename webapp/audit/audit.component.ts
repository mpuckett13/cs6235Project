import {Component, OnInit} from '@angular/core';

import {AuditService} from './audit.service';

@Component({
  moduleId: module.id,
  selector: 'audit',
  templateUrl: 'audit.component.html',
  styleUrls: []
})

export class AuditComponent implements OnInit {

  audit: any = {}

  constructor(
    private auditService: AuditService
  ) {}

  approve(record: any): void {
    this.auditService
        .approveRecord(record)
        .then(() => {
          record.approved = true;
        });
  }

  deny(record: any): void {
    this.auditService
        .denyRecord(record)
        .then(() => {
          record.denied = true;
        });
  }

  ngOnInit(): void {
    this.auditService
        .getForCurrentUser()
        .then(audit => {
          this.audit = audit;
        });
  }
}