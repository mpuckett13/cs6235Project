import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';

import {AuthService} from '../auth/auth.service';

@Injectable()
export class AuditService {

  private baseUrl = 'api/audit';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(
    private http: Http,
    private authService: AuthService
  ) {}

  getForCurrentUser(): Promise<any> {
    let userId = this.authService.getLocalUser().id;
    let headers = Object.assign({ userId: userId }, this.headers);

    return this.http
        .get(this.baseUrl, { headers: headers })
        .toPromise()
        .then(response => response.json())
        .catch(err => {
          console.error(err);
        });
  }

  approveRecord(record: any): Promise<any> {
    let userId = this.authService.getLocalUser().id;
    let headers = Object.assign({ userId: userId }, this.headers);

    return this.http
        .post(this.baseUrl + '/approve', { message: record }, { headers: headers })
        .toPromise();
  }

  denyRecord(record: any): Promise<any> {
    let userId = this.authService.getLocalUser().id;
    let headers = Object.assign({ userId: userId }, this.headers);

    return this.http 
        .post(this.baseUrl + '/deny', { message: record }, { headers: headers })
        .toPromise();
  }
}