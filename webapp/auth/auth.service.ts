import {Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';

import {User} from '../domain/User';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {

  private USER_ID_KEY = 'tsg.user';

  private baseUrl = 'api/authentication';
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {}

  authenticate(code: String): Promise<void> {
    return this.http
        .post(this.baseUrl, { code: code }, this.headers)
        .toPromise()
        .then((response: Response) => {
          this.storeUser(response.json() as User);
        })
        .catch(this.handleError);
  }

  attemptLogIn(): void {
    if (!this.getLocalUser()) {
      return;
    }

    const url = `${this.baseUrl}/${this.getLocalUser().id}`;
    
    this.http
        .get(url, this.headers)
        .toPromise()
        .then((response: Response) => {
          if (!response.ok) {
            this.clearUser();
          }
        })
        .catch(err => {
          if (err.status === 500) {
            this.clearUser();
          }
        });
  }

  isLoggedIn(): boolean {
    return !!this.getLocalUser();
  }

  getLocalUser(): User {
    return JSON.parse(localStorage.getItem(this.USER_ID_KEY));
  }

  private storeUser(user: User): void {
    localStorage.setItem(this.USER_ID_KEY, JSON.stringify(user));
  }

  private clearUser(): void {
    localStorage.removeItem(this.USER_ID_KEY);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}