export interface Blacklist {
  id: string,
  userId: string,
  list: string[]
}