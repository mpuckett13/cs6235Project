import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {BlacklistService} from './blacklist.service';

@Component({
  moduleId: module.id,
  selector: 'blacklist',
  templateUrl: 'blacklist.component.html',
  styleUrls: []
})

export class BlacklistComponent implements OnInit {
  blacklist: any = {};
  entry: string = null;

  constructor(
    private blacklistService: BlacklistService,
    private location: Location
  ) {}

  getBlacklist(): void {
    this.blacklistService
        .getForCurrentUser()
        .then(blacklist => {
          this.blacklist = blacklist;

          if (!this.blacklist.list) {
            this.blacklist.list = [];
          }
        });
  }

  addEntry(): void {
    if (this.entry) {
      this.blacklist.list.push(this.entry);
      this.entry = null;
      this.updateBlacklist();
    }
  }

  removeEntry(entry: string): void {
    let entryIndex = this.blacklist.list.indexOf(entry);
    this.blacklist.list.splice(entryIndex, 1);
    this.updateBlacklist();
  }

  private updateBlacklist(): void {
    this.blacklistService
        .update(this.blacklist)
        .then(() => {})
        .catch(() => {
          window.alert('Oops...we goofed');
        });
  }

  ngOnInit(): void {
    this.getBlacklist();
  }
}