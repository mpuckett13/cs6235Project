import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {AuditComponent} from './audit/audit.component';
import {AuditService} from './audit/audit.service';
import {AuthComponent} from './auth/auth.component';
import {AuthService} from './auth/auth.service';
import {DashboardComponent} from './dashboard.component';
import {BlacklistComponent} from './blacklist/blacklist.component';
import {BlacklistService} from './blacklist/blacklist.service';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full'
      },
      {
        path: 'auth/',
        component: AuthComponent
      },
      {
        path: 'audit',
        component: AuditComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'blacklist',
        component: BlacklistComponent
      },
    ])
  ],
  declarations: [
    AppComponent,
    AuditComponent,
    AuthComponent,
    BlacklistComponent,
    DashboardComponent
  ],
  providers: [
    AuditService,
    AuthService,
    BlacklistService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {

  constructor(
    private authService: AuthService
  ) {
    this.authService.attemptLogIn();
  }
}