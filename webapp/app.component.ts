import { Component } from '@angular/core';

import {AuthService} from './auth/auth.service';

@Component({
  moduleId: module.id,
  selector: 'my-app',
  templateUrl: 'app.component.html',
  styleUrls: [ 'app.component.css' ]
})

export class AppComponent {
  title = 'TSG Bot'

  constructor(
    private authService: AuthService
  ) {}

  showSlackButton(): boolean {
    return !this.authService.isLoggedIn();
  }
}
