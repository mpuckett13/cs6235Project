(function() {
  'use strict';

  var user = function(user) {
    this.user = user;
  };

  user.prototype.toClient = function() {
    return {
      name: this.user.userName,
      id: this.user._id
    };
  };

  module.exports = user;
})();