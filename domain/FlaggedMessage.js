(function () {
  'use strict';

  var FlaggedMessage = function(slackMessage) {
    this.commenterId = slackMessage.user;
    this.commenterName = null;
    this.channelId = slackMessage.channel;
    this.channelName = null;
    this.text = slackMessage.text;
    this.ts = slackMessage.ts;
  }

  module.exports = FlaggedMessage;
})();