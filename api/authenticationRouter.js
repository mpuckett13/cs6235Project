(function() {
  'use strict';

  const express = require('express');

  const authService = require('../services/authentication.js');
  
  const router = express.Router();

  router.post('/', (req, res) => {
    authService
        .submitSlackAuthRequest(req.body.code)
        .then(user => {
          res.status(201)
              .send(user.toClient());
        })
        .catch(err => { throw err; });
  });

  router.get('/:userId', (req, res) => {
    authService
        .getUserById(req.params.userId)
        .then(user => {
          if (!user) {
            res.status(404).end();
          } else { 
            res.status(200).send(user.toClient());
          }
        })
        .catch(err => {
          res.status(500).end();
          console.error(err);
        });
  });

  module.exports = router;
})();